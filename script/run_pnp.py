#!/usr/bin/python

import sys
import os
import math

import utils

InitPoses = { 'DISB1': '11.8 2 270', 'DISlabs': '9.7 7.3 90', \
  'Rive1': '10 18 0', 'Rockin2014': '-3.5 6.2 270', 
  'peccioli@Home' : '10 5.0 135', 'dis_corridor_office011': '14.0 8.3 0.0', 'peccioli05' : '18 13.6 0' }


def usage():
    print '  Use: ',sys.argv[0],' <robot_name> <map_name> <REAL|SIM> ', utils.args_help()
    print "  e.g. ",sys.argv[0],' diago DISB1 SIM'


if __name__ == '__main__':
  
  if (len(sys.argv)<4):
    usage()
    sys.exit()
    
    # Default values
  params = {}
  params['use_rgbd'] = False
  params['show_rviz'] = False
  
  params['robotname'] = sys.argv[1]
  params['mapname'] = sys.argv[2]
  params['mode'] = sys.argv[3]

  mp = params['mapname']
  if (mp in InitPoses.keys()):
    initpose = InitPoses[mp]
    pp = initpose.split()
    params['init_x'] = float(pp[0])
    params['init_y'] = float(pp[1])
    params['init_th'] = float(pp[2]) # deg   
  else:
    params['init_x'] = 0
    params['init_y'] = 0
    params['init_th'] = 0 # deg

  # Read command-line arguments
  utils.readargs(sys.argv,params)
  
  # Start the robot (base and sensors)
  utils.start_robot(params)

  init_th_rad = params['init_th']*math.pi/180.0 # rad
  
  # Start the navigation nodes (localizer, navigation,...)
  cmd = 'xterm -e roslaunch diago_apps diago_nav.launch robot_name:=%s map_name:=%s use_joystick:=true use_srrg_localizer:=true initial_pose_x:=%f  initial_pose_y:=%f initial_pose_a:=%f use_move_base:=false &' % (params['robotname'], params['mapname'], params['init_x'], params['init_y'], init_th_rad)
  print cmd
  os.system(cmd)
  os.system('sleep 5')  

  # Start PNP (pnp_ros, hri_pnp)
  cmd = 'xterm -e roslaunch hri_pnp hri_pnpas.launch robot_name:=%s &'  % (params['robotname'])
  print cmd
  os.system(cmd)
  os.system('sleep 2')  

  # Start leg tracker
  cmd = 'xterm -e roslaunch detector_msg_to_map leg_tracker_only_laser.launch robot_name:=%s &'  % (params['robotname'])

  print cmd
  os.system(cmd)
  os.system('sleep 2')  

  # Start joy_events (pnp_ros, hri_pnp)
  cmd = 'xterm -e $SPQREL_HOME/src/hri_pnp/scripts/joyevents.py &'
  print cmd
  os.system(cmd)
  os.system('sleep 2')  

  # Start rviz
  if (params['show_rviz']):
    utils.start_rviz()
    

