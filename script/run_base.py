#!/usr/bin/python

import sys
import os
import math

import utils


def usage():
    print "  Use: ",sys.argv[0],' <robot_name> <REAL|SIM> ', utils.args_help()
    print "  e.g. ",sys.argv[0],' diago SIM'


if __name__ == '__main__':
  
  if (len(sys.argv)<3):
    usage()
    sys.exit()

  # Default values
  params = {}
  
  params['use_rgbd'] = False
  params['show_rviz'] = False
  params['robotname'] = sys.argv[1]
  params['mode'] = sys.argv[2]
  params['mapname'] = 'DISB1' # needed for SIM mode
  params['init_x'] = 11.8
  params['init_y'] = 2
  params['init_th'] = 270 # deg

  # Read command-line arguments
  utils.readargs(sys.argv,params)
  
  # Start the robot (base and sensors)
  utils.start_robot(params)

  os.system('sleep 5')  

  # Start rviz
  if (params['show_rviz']):
    utils.start_rviz()

