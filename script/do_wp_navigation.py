#!/usr/bin/env python

import os

if __name__ == '__main__':
    robotname = 'diago'
    turn_str = 'rosrun rococo_navigation turn_node -client %s ' % (robotname)
    followcorridor_str = 'rosrun rococo_navigation follow_corridor_node -client %s ' % (robotname)
    goto_str = 'rosrun rococo_navigation gotopose_client %s ' % (robotname)
    navgoal_str = "rostopic pub /%s/move_base_simple/goal geometry_msgs/PoseStamped '{header: {frame_id: \"/map\"}, pose: {position: {x: %f, y: %f, z: 0.0}, orientation: { x: 0.0, y: 0.0, z: %f, w: 1.0}}}' --once" % (robotname, 6.0, 17.0, 3.14)


    os.system("rosparam set /diago/gradientBasedNavigation/force_scale 0.4")
    os.system("rosparam set /diago/gradientBasedNavigation/momentum_scale 0.3")

    os.system(goto_str+"9.0 7.0 90")

    os.system("rosparam set /diago/gradientBasedNavigation/force_scale 0.6")
    os.system("rosparam set /diago/gradientBasedNavigation/momentum_scale 0.2")


    for i in range(1,3):
        print "Iteration ",i
        os.system(followcorridor_str+"9.0 17.5 1.0")    
        os.system(turn_str+"180 ABS")    
        os.system(followcorridor_str+"5.7 17.5 1.0")
        #os.system(goto_str+"6.0 17.0 180")

        os.system("rosparam set /diago/gradientBasedNavigation/force_scale 0.4")
        os.system("rosparam set /diago/gradientBasedNavigation/momentum_scale 0.3")

        os.system(goto_str+"3.0 18.0 180")
        os.system(turn_str+"180 REL")    
        os.system(goto_str+"6.5 17.0 270")

        os.system("rosparam set /diago/gradientBasedNavigation/force_scale 0.6")
        os.system("rosparam set /diago/gradientBasedNavigation/momentum_scale 0.2")
      
        os.system(followcorridor_str+"5.7 5.0 1.0")

        os.system(goto_str+"3.5 3.5 180")
        os.system(goto_str+"9.0 7.0 90")

