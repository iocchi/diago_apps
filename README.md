# diago_apps #

This package contains scripts to start demos/apps on Diago.

Main script to run:

```
#!text
start_demo.py 
```

You can select SIM or REAL robot, which device are enabled (for REAL robot),
map and demo.

Notes for use of joystick:

```
#!text
  Use Logitech joystick, make sure the setting D/X is on D.
  Left pad for speed, right pad for jog.
  Joystick buttons:
    RB: emergency stop
    RT: release emergency stop
    LB: joystick control - other controllers disabled
    LT: release joystick control - other controllers enabled
```
    
Notes about Kinect:
*  By default, Kinect drivers are not launched!
*  If you want Kinect drivers running, use the option "-enable-rgbd" to any of the script described below.



# Demos available #

IMPORTANT NOTES:
- Use diago as robot_name. 
- REAL starts the demo on the real robot, SIM on Stage simulator.
- The robot in simulation is always localized, while the real robot must be manually localized, either by start the app from the nominal initial position of the robot or by manually localize it with rviz.
- Additional command-line options for all the scripts below:
```
#!text
  [-keyboard] [-enable-rgbd] [-nogbn] [-initpose X Y Th(deg)] [-rviz]
```


### Base+laser only ###

Command line start

```
#!text
script$  ./run_base.py <robot_name> <REAL|SIM> 
```
  
Nodes and topics generated in SIM mode:
  
```
#!text
$ rosnode list

/rosout
/stageros

$ rostopic list 

/clock
/diago/base_pose_ground_truth
/diago/cmd_vel
/diago/odom
/diago/scan
/rosout
/rosout_agg
/stageGUIRequest
/tf
```

  
Nodes and topics generated in REAL mode
  
```
#!text
$ rosnode list

/diago/front_laser_frame_broadcaster
/diago/hokuyo_front
??? /diago/hokuyo_back ???
/diago/rear_laser_link_broadcaster
/diago/segway_rmp_node

$ rostopic list 

/diagnostics
/diago/cmd_vel
/diago/hokuyo_urg04/parameter_descriptions
/diago/hokuyo_urg04/parameter_updates
/diago/hokuyo_utm30/parameter_descriptions
/diago/hokuyo_utm30/parameter_updates
/diago/odom
/diago/rear_scan
/diago/scan
/diago/segway_status
/rosout
/rosout_agg
/tf
```


### Base + laser + rgbd cameras ### 

Available only in REAL mode

Command line start

```
#!text
script$  ./run_base.py <robot_name> <REAL|SIM> -enable-rgbd
```

Additional nodes

```
#!text
/diago/thin_kinect_bottom
/diago/thin_kinect_top
```

Additional topics
  
```
#!text
/diago/bottom_camera/depth/camera_info
/diago/bottom_camera/depth/image_raw
/diago/bottom_camera/rgb/camera_info
/diago/bottom_camera/rgb/image_raw
/diago/top_camera/depth/camera_info
/diago/top_camera/depth/image_raw
/diago/top_camera/rgb/camera_info
/diago/top_camera/rgb/image_raw
```


To check data from sensors:
    
```
#!text
script$ rosrun rviz rviz -d ../../diago_setup/config/rviz/diago.rviz 
```

To check Kinect cameras only
  
```
#!text
$ rosrun thin_kinect thin_kinect_node _rgb_mode:=1 _depth_mode:=4 _device_id:=A00363A41661035A
```

on diago: A00363A19219144A (top), A00363A41661035A (bottom) 

    
### Joystick/Keyboard ###

Drive the robot with a joystick/keyboard using gradient-based navigation.

Command line start:
   
```
#!text
script$  ./run_joystick.py <robot_name> <REAL|SIM> 
```
   
Rviz (starts automatically for REAL robot):
    
```
#!text
$ xterm -e rosrun rviz rviz -d `rospack find diago_setup`/config/rviz/diago.rviz &
```

(change the fixed frame if needed)

To run base + rgbd cameras + joystick use:

```
#!text
script$  ./run_joystick.py <robot_name> REAL -enable-rgbd
```

     
### move_base ###

Drive the robot using move_base. No joystick control, no gradient based navigation.
Make sure the robot is localized or localize it manually with rviz.
Use rviz to send target goals.

Command line start

```
#!text
script$  ./run_movebase.py  <robot_name> <map_name> <REAL|SIM> 
```


TODO: Test on the robot!!!
    

    
### Navigation (patrol/follow person) ###

Drive the robot with follow_corridor and follow_person actions

Gradient based navigation and joystick control active

Command line start

```
#!text
script$  ./run_navigation.py  <robot_name> <map_name> <REAL|SIM> 
```

Application start (choose one):
```
#!text    
script$  xterm -e ./do_patrol.py &
```
The robot moves on a predefined path
        
```
#!text
script$  xterm -e ./do_follow_person.py &
```

The robot follows the person in front of it (one direction only).
        
During the navigation, joystick can be used
        

TODO: Test on the robot!!!


### PNP plan execution ###

Command line start

```
#!text
script$  ./run_pnp.py  <robot_name> <map_name> <REAL|SIM> 
```

Execution of a plan:

```
#!text
$  rostopic pub /diago/planToExec std_msgs/String "data: 'my_plan'" --once
```

The plan (my_plan.pnml file) must be in the folder specified in hri_pnp/launch/hri_pnpas.launch
(default is hri_pnp/plans)


Stopping the current plan:

```
#!text
$  rostopic pub /diago/planToExec std_msgs/String "data: 'stop'" --once
```

Restarting the current plan:

```
#!text
$  rostopic pub /diago/planToExec std_msgs/String "data: '<currentplan>'" --once
```

For more information about generation and execution of plans, see the README file in hri_pnp.




